name             'gitlab_omnibus'
maintainer       'Drew Blessing'
maintainer_email 'drew.blessing@mac.com'
license          'Apache-2.0'
description      'Installs/Configures GitLab and GitLab CI Omnibus'
source_url       'https://gitlab.com/dblessing/chef-gitlab_omnibus'
issues_url       'https://gitlab.com/dblessing/chef-gitlab_omnibus/issues'

version          '2.0.0'

# This is needed to use the osl_package_cloud resource
depends 'base'

supports         'centos', '~> 7.0'
supports         'centos', '~> 8.0'
