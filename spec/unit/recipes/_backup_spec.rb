require 'spec_helper'

describe 'gitlab_omnibus::_backup' do
  ALL_PLATFORMS.each do |p|
    context "#{p[:platform]} #{p[:version]}" do
      cached(:chef_run) do
        ChefSpec::SoloRunner.new(p).converge(described_recipe)
      end
      it do
        is_expected.to create_cron('backup_gitlab')
          .with_command('CRON=1 /opt/gitlab/bin/gitlab-rake gitlab:backup:create')
          .with_minute('0')
          .with_hour('3')
          .with_day('*')
          .with_month('*')
          .with_weekday('*')
      end
    end
  end
end
