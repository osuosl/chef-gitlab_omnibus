require 'spec_helper'

describe 'gitlab_omnibus::default' do
  ALL_PLATFORMS.each do |p|
    context "#{p[:platform]} #{p[:version]}" do
      cached(:chef_run) do
        ChefSpec::SoloRunner.new(p).converge(described_recipe)
      end

      before do
        allow(File).to receive(:exist?).and_call_original
        allow(File).to receive(:exist?).with('/opt/gitlab/bin/gitlab-ctl').and_return(false)
      end

      it { is_expected.to install_package('gitlab-ce').with_version(nil) }
      it { is_expected.to include_recipe('gitlab_omnibus::_backup') }

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).to notify('execute[gitlab_reconfigure]').to(:run).immediately
      end

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).not_to notify('execute[gitlab_restart]').to(:run).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).not_to notify('execute[gitlab_reconfigure]').to(:run).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).not_to notify('execute[gitlab_restart]').to(:run).delayed
      end

      it do
        is_expected.to render_file('/etc/gitlab/gitlab.rb')
          .with_content(
"# This file is managed by Chef, using the gitlab_omnibus cookbook.
# Editing this file by hand is highly discouraged!

external_url 'https://fauxhai.local'")
      end

      context 'on subsequent runs' do
        before do
          allow(File).to receive(:exist?).and_call_original
          allow(File).to receive(:exist?).with('/opt/gitlab/bin/gitlab-ctl').and_return(true)
        end
        cached(:chef_run) do
          ChefSpec::SoloRunner.new(p).converge(described_recipe)
        end

        it do
          expect(
            chef_run.template('/etc/gitlab/gitlab.rb')
          ).to notify('execute[gitlab_reconfigure]').to(:run).delayed
        end

        it do
          expect(
            chef_run.template('/etc/gitlab/gitlab.rb')
          ).to notify('execute[gitlab_restart]').to(:run).delayed
        end

        it do
          expect(
            chef_run.package('gitlab-ce')
          ).to notify('execute[gitlab_reconfigure]').to(:run).delayed
        end

        it do
          expect(
            chef_run.package('gitlab-ce')
          ).to notify('execute[gitlab_restart]').to(:run).delayed
        end
      end
    end
  end
end
