#
# Cookbook:: gitlab_omnibus
# Recipe:: default
#
# Copyright:: 2015-2020, Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if node['gitlab_omnibus']['use_packagecloud']
  case node['gitlab_omnibus']['edition']
  when 'community'
    osl_packagecloud_repo 'gitlab/gitlab-ce' do
      base_url 'https://packages.gitlab.com'
    end

    node.default['gitlab_omnibus']['package_name'] = 'gitlab-ce'
  when 'enterprise'
    osl_packagecloud_repo 'gitlab/gitlab-ee' do
      base_url 'https://packages.gitlab.com'
    end
    node.default['gitlab_omnibus']['package_name'] = 'gitlab-ee'
  end
end

# Low tech way to prevent unnecessary reconfigure and restart actions later on
first_install = !File.exist?('/opt/gitlab/bin/gitlab-ctl')

# gitlab_omnibus_package 'gitlab' do
package node['gitlab_omnibus']['package_name'] do
  action node['gitlab_omnibus']['action']
  version node['gitlab_omnibus']['version']

  # No need to reconfigure or restart on first install.
  unless first_install
    notifies :run, 'execute[gitlab_reconfigure]'
    notifies :run, 'execute[gitlab_restart]'
  end
end

template '/etc/gitlab/gitlab.rb' do
  source 'gitlab.rb.erb'
  mode '0644'

  # This is pretty crazy conditional logic just for reconfigure/restart
  # but there are lots of edge cases to cover.
  if first_install
    # Must reconfigure immediately on first install otherwise service will fail to start
    notifies :run, 'execute[gitlab_reconfigure]', :immediately

    # If CI is enabled we need an extra reconfigure to generate oauth configuration
    # once CI and Gitlab are both up and running.
    if node['gitlab_omnibus']['registry_external_url']
      notifies :run, 'execute[gitlab_reconfigure]'
    end
  else
    notifies :run, 'execute[gitlab_reconfigure]'
    notifies :run, 'execute[gitlab_restart]'
  end
end

execute 'gitlab_reconfigure' do
  command '/opt/gitlab/bin/gitlab-ctl reconfigure'
  action :nothing
end

execute 'gitlab_restart' do
  command '/opt/gitlab/bin/gitlab-ctl restart'
  action :nothing
end

if node['gitlab_omnibus']['backup']['enable']
  include_recipe 'gitlab_omnibus::_backup'
end
